#include <stdio.h>

#include "image.h"

#ifndef BMP_H
#define BMP_H

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR = 10,
    WRITE_HEADER_ERROR = 20
};




enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 6,
    READ_INVALID_BITS = 7,
    READ_INVALID_HEADER = 8,
    READ_ERROR = 9
};


enum read_status from_bmp(FILE *source_file, struct image *image);
enum write_status to_bmp(FILE *target_file, struct image const *image);

#endif
