#include <stdio.h>

#ifndef FILE_OPEN_H
#define FILE_OPEN_H


enum file_open_status {
    FILE_OPEN_OK = 0,
    FILE_OPEN_NO_ACCESS = 4,
    FILE_OPEN_NOT_EXIST = 5
};


enum file_open_type {
    FILE_OPEN_READ_ONLY = 0,
    FILE_OPEN_READ_AND_WRITE
};

enum file_open_status file_open(char* filename, FILE** file, enum file_open_type type);

#endif
