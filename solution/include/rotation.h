#include "image.h"

#ifndef ROTATION_H
#define ROTATION_H

enum rotation_status {
    ROTATION_OK = 0,
    ROTATION_ERROR = 11
};

enum rotation_status rotate(struct image const* source_image, struct image* target_image, long angle);

#endif
