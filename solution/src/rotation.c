#include "rotation.h"
#include <malloc.h>
#include <string.h>


// Safe memory copy function
static void safe_copy(void* dest, const void* src, size_t n) {
    if (dest == NULL || src == NULL) return;
    char *d = dest;
    const char *s = src;
    while (n--) {
        *d++ = *s++;
    }
}

static enum rotation_status copy_image(const struct image* source_image, struct image* target_image) {
    if (target_image->data) {
         free(target_image->data);
    }

    if (!source_image || !target_image) return ROTATION_ERROR;

    target_image->width = source_image->width;
    target_image->height = source_image->height;

    size_t data_size = sizeof(struct pixel) * target_image->width * target_image->height;
    target_image->data = malloc(data_size);
    if (!target_image->data) {
        return ROTATION_ERROR;
    }

    safe_copy(target_image->data, source_image->data, data_size);
    return ROTATION_OK;
}

static void rotate_90deg(struct image* image) {
    if (!image || !image->data) {
        fprintf(stderr, "Error: Invalid image data.\n");
        return;
    }

    struct image rotated_image = {
            .width = image->height,
            .height = image->width,
            .data = malloc(sizeof(struct pixel) * image->height * image->width)
    };

    if (!rotated_image.data) {
        free(image->data);
        image->data = NULL;
        return;
    }

    for (uint32_t y = 0; y < rotated_image.height; y++) {
        for (uint32_t x = 0; x < rotated_image.width; x++) {
            rotated_image.data[rotated_image.width * y + x] =
                    image->data[image->width * x + (image->width - y - 1)];
        }
    }

    free(image->data);
    *image = rotated_image;
}

enum rotation_status rotate(const struct image* source_image, struct image* target_image, long angle) {
    if (!source_image || !target_image) return ROTATION_ERROR;

    if (copy_image(source_image, target_image) != ROTATION_OK) {
        return ROTATION_ERROR;
    }

    // Normalize angle to the range [0, 360)
    angle = (angle % 360 + 360) % 360;
    long times = angle / 90;

    for (long i = 0; i < times; i++) {
        rotate_90deg(target_image);
    }

    return ROTATION_OK;
}
