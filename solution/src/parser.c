#include "parser.h"
#include <stdlib.h>

#define PROGRAM_ARGUMENT_COUNT 4
#define ALLOWED_ANGLE_COUNT 7

enum parse_status parse(int argc, char** argv, char** source_filename, char** target_filename, long* angle) {
    // Check for correct number of arguments
    if (argc != PROGRAM_ARGUMENT_COUNT) {
        return PARSE_NOT_REQUIRED_ARGUMENTS_COUNT;
    }

    // Check if angle is a valid integer
    char* angle_str = argv[3];
    char* err_ptr;
    *angle = strtol(angle_str, &err_ptr, 10);
    if (*err_ptr != '\0') {
        return PARSE_NON_NUMERIC_ANGLE;
    }

    // Define allowed angles
    long allowed_angles[ALLOWED_ANGLE_COUNT] = {0, 90, -90, 180, -180, 270, -270};

    // Check if angle is within allowed values
    int i;
    for (i = 0; i < ALLOWED_ANGLE_COUNT; i++) {
        if (*angle == allowed_angles[i]) {
            break;
        }
    }
    if (i == ALLOWED_ANGLE_COUNT) {
        return PARSE_PROHIBITED_ANGLE;
    }

    // Assign filenames
    *source_filename = argv[1];
    *target_filename = argv[2];

    return PARSE_OK;
}
