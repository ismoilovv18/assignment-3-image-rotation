#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "file_open.h"

enum file_open_status file_open(char* filename, FILE** file, enum file_open_type type) {
    if (!filename || !file) {
        fprintf(stderr, "Invalid input: filename or file pointer is NULL.\n");
        return FILE_OPEN_NOT_EXIST;
    }

    const char* mode;
    switch (type) {
        case FILE_OPEN_READ_ONLY:
            mode = "rb";
            break;
        case FILE_OPEN_READ_AND_WRITE:
            mode = "wb";
            break;
        default:
            fprintf(stderr, "Invalid file open type.\n");
            return FILE_OPEN_NOT_EXIST;
    }

    *file = fopen(filename, mode);
    if (!*file) {
        switch (errno) {
            case EACCES:
                fprintf(stderr, "No access to open file %s.\n", filename);
                return FILE_OPEN_NO_ACCESS;
            case ENOENT:
                fprintf(stderr, "File %s does not exist.\n", filename);
                return FILE_OPEN_NOT_EXIST;
            default:
                fprintf(stderr, "Error opening file %s: %s\n", filename, strerror(errno));
                return FILE_OPEN_NOT_EXIST;
        }
    }

    return FILE_OPEN_OK;
}
