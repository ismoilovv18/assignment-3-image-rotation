#include "bmp.h"
#include <stdlib.h>

#define BMP_SIGNATURE_HEX 0x4D42
#define BMP_RESERVED 0
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0
#define BMP_X_PELS_PER_METER 1
#define BMP_Y_PELS_PER_METER 1
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0
#define PADDING_BYTE 4

// Function to calculate padding
static uint8_t calculate_padding(uint32_t width) {
    return (PADDING_BYTE - (width * sizeof(struct pixel)) % PADDING_BYTE) % PADDING_BYTE;
}

static struct bmp_header create_bmp_header(struct image const* image) {
    struct bmp_header header = {0};

    uint8_t padding = calculate_padding(image->width);

    header.bfType = BMP_SIGNATURE_HEX;
    header.bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel) * image->width + padding) * image->height;
    header.bfReserved = BMP_RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_SIZE;
    header.biHeight = image->height;
    header.biWidth = image->width;
    header.biPlanes = BMP_PLANES;
    header.biBitCount = BMP_BIT_COUNT;
    header.biCompression = BMP_COMPRESSION;
    header.biSizeImage = (image->width + padding) * image->height;
    header.biXPelsPerMeter = BMP_X_PELS_PER_METER;
    header.biYPelsPerMeter = BMP_Y_PELS_PER_METER;
    header.biClrUsed = BMP_CLR_USED;
    header.biClrImportant = BMP_CLR_IMPORTANT;

    return header;
}

enum read_status from_bmp(FILE* source_file, struct image* image) {
    if (!source_file || !image) {
        return READ_ERROR;
    }

    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, source_file) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_SIGNATURE_HEX || header.biBitCount != BMP_BIT_COUNT) {
        return READ_INVALID_SIGNATURE;
    }

    image->width = header.biWidth;
    image->height = header.biHeight;

    image->data = malloc(sizeof(struct pixel) * image->width * image->height);
    if (!image->data) {
        return READ_ERROR;
    }

    uint8_t padding = calculate_padding(image->width);

    for (uint32_t y = 0; y < image->height; y++) {
        if (fread(&image->data[y * image->width], sizeof(struct pixel), image->width, source_file) != image->width) {
            free(image->data);
            return READ_ERROR;
        }
        fseek(source_file, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* target_file, struct image const* image) {
    if (!target_file || !image) {
        return WRITE_HEADER_ERROR;
    }

    struct bmp_header header = create_bmp_header(image);
    uint8_t padding = calculate_padding(image->width);

    if (fwrite(&header, sizeof(struct bmp_header), 1, target_file) != 1) {
        return WRITE_ERROR;
    }

    for (uint32_t y = 0; y < image->height; y++) {
        if (fwrite(&image->data[image->width * y], sizeof(struct pixel), image->width, target_file) != image->width) {
            return WRITE_ERROR;
        }
        fseek(target_file, padding, SEEK_CUR);
    }

    return WRITE_OK;
}
