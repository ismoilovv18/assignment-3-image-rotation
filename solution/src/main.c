#include "bmp.h"
#include "file_open.h"
#include "image.h"
#include "parser.h"
#include "rotation.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>


static enum program_status process_image(char* source_filename, char* target_filename, long angle) {
    // Проверка допустимости угла поворота
    if (angle % 90 != 0) { // Предположим, что допустимы только повороты на 90, 180, 270 градусов
        fprintf(stderr, "Error: Invalid rotation angle. Angle must be a multiple of 90.\n");
        return PROGRAM_ROTATION_ERROR;
    }

    FILE *source_file = NULL, *target_file = NULL;
    struct image source_image = {0}, target_image = {0};
    enum program_status status = PROGRAM_SUCCESS;

    // Open and read the source file
    if (file_open(source_filename, &source_file, FILE_OPEN_READ_ONLY) != FILE_OPEN_OK) {
        status = PROGRAM_FILE_OPEN_ERROR;
        goto cleanup;
    }

    if (from_bmp(source_file, &source_image) != READ_OK) {
        status = PROGRAM_BMP_READ_ERROR;
        goto cleanup;
    }

    // Rotate the image
    if (rotate(&source_image, &target_image, angle) != ROTATION_OK) {
        status = PROGRAM_ROTATION_ERROR;
        goto cleanup;
    }

    // Open and write to the target file
    if (file_open(target_filename, &target_file, FILE_OPEN_READ_AND_WRITE) != FILE_OPEN_OK) {
        status = PROGRAM_FILE_OPEN_ERROR;
        goto cleanup;
    }

    if (to_bmp(target_file, &target_image) != WRITE_OK) {
        status = PROGRAM_BMP_WRITE_ERROR;
    }

    cleanup:
    if (source_file) fclose(source_file);
    if (target_file) fclose(target_file);
    free(source_image.data);
    free(target_image.data);
    return status;
}

int main(int argc, char **argv) {
    char *source_filename, *target_filename;
    long angle;

    if (parse(argc, argv, &source_filename, &target_filename, &angle) != PARSE_OK) {
        fprintf(stderr, "Error: Argument parsing failed.\n");
        return PROGRAM_ARGUMENTS_PARSE_ERROR;
    }

    enum program_status status = process_image(source_filename, target_filename, angle);
    if (status != PROGRAM_SUCCESS) {
        fprintf(stderr, "Error: Processing image failed with status %d.\n", status);
        return status;
    }

    return PROGRAM_SUCCESS;
}
